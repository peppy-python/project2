class Person:
    def __init__(self, name):
        self.__person_name = name

    @property
    def name(self):
        return self.__person_name

    @name.setter    
    def name(self, name):
        self.__person_name = name


person = Person("Vasiliy")
print(person._Person__person_name)
print(person.name)
person.name = "Vasya"
print(person.name)
